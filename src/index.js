const mix = require('laravel-mix')

class Elm {
    /**
     * Rules to be merged with the master webpack loaders.
     *
     * @return {Array|Object}
     */
    webpackRules() {
        return {
            test: /\.elm$/,
            exclude: [/elm-stuff/, /node_modules/],
            loader: "elm-webpack-loader",
            options: {
                debug: ! mix.inProduction(),
                optimize: mix.inProduction()
            }
        }
    }
}

mix.extend('elm', new Elm())
