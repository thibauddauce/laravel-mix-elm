# Laravel Mix Elm

Compile [Elm files](https://elm-lang.org/) with [Laravel Mix](https://github.com/JeffreyWay/laravel-mix).

## Installation

```bash
npm install laravel-mix-elm --save
```

## Usage

In your `webpack.mix.js`:

```js
let mix = require('laravel-mix')
require('laravel-mix-elm')

mix.js('resources/js/app.js', 'public/js')
   .elm()
```

In your `app.js`:

```js
let { Elm } = require('./Main.elm')

let app = Elm.Main.init({
    node: document.getElementById('main')
})
```
